#ifndef ARGS_H
#define ARGS_H

typedef enum {
    NEW,        // create a project
    BUILD,      // build a project
    RUN         // build and run a project or run a project (is_built is denoted in .vendettaconfig => if true, just run, otherwise build and run)
} meg_e; // mutually_exclusive_group_e;

typedef struct {
    meg_e main_arg;
    char* project_name;
} args_t;


args_t parse_args(int argc, char** argv);

#endif
