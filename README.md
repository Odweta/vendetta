# Vendetta

A manager/creator for projects that make use of the OdwetaLibC library

It creates a project structure that can be seen in `/example_project/`.

## Conventions

Vendetta uses **git** as the version control system and **make** as the build system which uses **gcc** to compile all the glorious C code your project has to offer. Let's say the root of your project directory is `/`. Then all of your source files go in `/src/`, all of your includes go in `/include/`. And that's all you need to know, I guess. Just submit an issue when you don't know what to do... All your Makefiles, .gits, READMEs, LICENSEs and stuff like that are in `/`.

## Installation

`make install`

## Create a project

`vendetta new <project_name>`

## Build a project

`vendetta build`

## Build and run a project

`vendetta run`
