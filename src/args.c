#include "../include/args.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

args_t parse_args(int argc, char** argv) {
    if (argc < 2 || argc > 3) {
        fprintf(stderr, "invalid number of arguments\n");
        exit(1);
    }

    args_t args;

    if (strcmp(argv[1], "new") == 0) {
        args.main_arg = NEW;
        if (argc != 3) {
            fprintf(stderr, "not enough arguments supplied, supply a project name\n");
            exit(1);
        } // TODO: add licenses to newly created projects
        args.project_name = (char*)malloc(sizeof(argv[2]));
        strcpy(args.project_name, argv[2]);
    } else if (strcmp(argv[1], "build") == 0) {
        args.main_arg = BUILD;
    } else if (strcmp(argv[1], "run") == 0) {
        args.main_arg = RUN;
    } else {
        fprintf(stderr, "invalid argument\n");
        exit(1);
    }

    return args;
}
