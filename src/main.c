#include "../include/args.h"
#include "../include/build.h"
#include "../include/new.h"
#include "../include/run.h"

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv) {
  args_t args = parse_args(argc, argv);

  switch (args.main_arg) {
  case NEW:
    new_project(args.project_name);
    break;
  case BUILD:
    build_project();
    break;
  case RUN:
    run_project();
    break;
  }

  free(args.project_name);

  return 0;
}
