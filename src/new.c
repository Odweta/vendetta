#include "../include/new.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct {
  char *main_c;
  char *makefile;
  char *readme;
} thing_t;

void main_c(char *name, char *path) {
  FILE *fd = fopen(path, "w");
  if (!fd) {
    fprintf(stderr, "could not open main.c\n");
    exit(1);
  }

  fprintf(fd, "#include <stdio.h>\n\nint main() {\n    printf(\"Hello, "
              "world!\\n\");\n}\n");

  fclose(fd);
}

void makefile(char *name, char *path) {
  FILE *fd = fopen(path, "w");
  if (!fd) {
    fprintf(stderr, "could not open Makefile\n");
    exit(1);
  }

  char *payload = "\
CC := gcc\n\
CFLAGS := -Wall -Werror\n\
LIBS := -lvector -lfile -llist -lutil\n\
LIBSPATH := -L/usr/lib/odweta/\n\
INCLUDEDIR := -I/usr/include/odweta/\n\
PATCH := -Wl,-rpath=/usr/lib/odweta/\n\
\n\
.PHONY: build run\n\
\n\
default: build\n\
\n\
build: src/main.c # add files to compile here\n\
\t@$(CC) $(CFLAGS) -o ./build/$@ $^ $(PATCH) $(INCLUDEDIR) $(LIBSPATH) $(LIBS)\n\
\n\
run:\n\
\t@./build/main\n";

  fprintf(fd, "%s", payload);

  fclose(fd);
}

void readme(char *name, char *path) {
  FILE *fd = fopen(path, "w");
  if (!fd) {
    fprintf(stderr, "could not open README.md\n");
    exit(1);
  }

  char readme_text[strlen("# ") + strlen(name) +
                   strlen("\n\nYour project description goes here\n") + 1];
  strcpy(readme_text, "# ");
  strcat(readme_text, name);
  strcat(readme_text, "\n\nYour project description goes here\n");

  fprintf(fd, "%s", readme_text);

  fclose(fd);
}

void fill_in_files(char *name, thing_t thing) {
  main_c(name, thing.main_c);
  makefile(name, thing.makefile);
  readme(name, thing.readme);
  // license(name);
}

void dirs(char *name) {
  char mkdir_str[strlen("mkdir -p ./") + strlen(name) + strlen("/src") + 1];
  strcpy(mkdir_str, "mkdir -p ./");
  strcat(mkdir_str, name);
  strcat(mkdir_str, "/src");

  system(mkdir_str);

  char
      mkdir_str2[strlen("mkdir -p ./") + strlen(name) + strlen("/include") + 1];
  strcpy(mkdir_str2, "mkdir -p ./");
  strcat(mkdir_str2, name);
  strcat(mkdir_str2, "/include");

  system(mkdir_str2);

  char mkdir_str3[strlen("mkdir -p ./") + strlen(name) + strlen("/build") + 1];
  strcpy(mkdir_str3, "mkdir -p ./");
  strcat(mkdir_str3, name);
  strcat(mkdir_str3, "/build");

  system(mkdir_str3);
}

void files(char *name) {
  char smain_c[] = "/src/main.c";
  char smakefile[] = "/Makefile";
  char sreadme[] = "/README.md";
  char slicense[] = "/LICENSE";
  char sgitignore[] = "/.gitignore";

  char touch1[strlen("touch ./") + strlen(name) + strlen(smain_c) + 1];
  char touch2[strlen("touch ./") + strlen(name) + strlen(smakefile) + 1];
  char touch3[strlen("touch ./") + strlen(name) + strlen(sreadme) + 1];
  char touch4[strlen("touch ./") + strlen(name) + strlen(slicense) + 1];
  char touch5[strlen("touch ./") + strlen(name) + strlen(sgitignore) + 1];

  strcpy(touch1, "touch ./");
  strcat(touch1, name);
  strcat(touch1, smain_c);

  strcpy(touch2, "touch ./");
  strcat(touch2, name);
  strcat(touch2, smakefile);

  strcpy(touch3, "touch ./");
  strcat(touch3, name);
  strcat(touch3, sreadme);

  strcpy(touch4, "touch ./");
  strcat(touch4, name);
  strcat(touch4, slicense);

  strcpy(touch5, "touch ./");
  strcat(touch5, name);
  strcat(touch5, sgitignore);

  system(touch1);
  system(touch2);
  system(touch3);
  system(touch4);
  system(touch5);
}

thing_t create_files(char *name) {
  char main_c_path[2 + strlen(name) + 11 + 1];
  char makefile_path[2 + strlen(name) + 9 + 1];
  char readme_path[2 + strlen(name) + 10 + 1];

  strcpy(main_c_path, "./");
  strcpy(makefile_path, "./");
  strcpy(readme_path, "./");

  thing_t thing;

  thing.main_c = (char *)malloc(sizeof(main_c_path));
  thing.makefile = (char *)malloc(sizeof(makefile_path));
  thing.readme = (char *)malloc(sizeof(readme_path));

  strcat(main_c_path, name);
  strcat(main_c_path, "/src/main.c");

  strcat(makefile_path, name);
  strcat(makefile_path, "/Makefile");

  strcat(readme_path, name);
  strcat(readme_path, "/README.md");

  strcpy(thing.main_c, main_c_path);
  strcpy(thing.makefile, makefile_path);
  strcpy(thing.readme, readme_path);

  dirs(name);

  files(name);

  return thing;
}

void new_project(char *name) {
  thing_t thing = create_files(name);

  fill_in_files(
      name,
      thing); // fills in stuff like the Makefile with some boilerplate text

  free(thing.main_c);
  free(thing.makefile);
  free(thing.readme);
}
