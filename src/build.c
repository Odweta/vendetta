#include "../include/build.h"

#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define MAX(n) n
int src_files_num;

int count_src_files(char *src_dir) {
  int i = 0;

  DIR *d;
  struct dirent *dir;
  d = opendir(src_dir); // all source (.c) files
  if (d) {
    while ((dir = readdir(d)) != NULL) {
      if (dir->d_type == DT_REG) {
        i++;
      }
    }
    closedir(d);
  }

  return i;
}

void load_filenames(char src_files[][1000], char *src_dir) {
  int i = 0;

  DIR *d;
  struct dirent *dir;
  d = opendir(src_dir); // all source (.c) files
  if (d) {
    while ((dir = readdir(d)) != NULL) {
      if (dir->d_type == DT_REG) {
        if (i < src_files_num) {
          strcpy(src_files[i++], dir->d_name);
        }
      }
    }
    closedir(d);
  }
}

char *create_new_makefile(char src_files[][1000]) {
  char makefile[999999]; // idk how many
  strcpy(makefile, "\
CC := gcc\n\
CFLAGS := -Wall -Werror\n\
LIBS := -lvector -lfile -llist -lutil\n\
LIBSPATH := -L/usr/lib/odweta/\n\
INCLUDEDIR := -I/usr/include/odweta/\n\
PATCH := -Wl,-rpath=/usr/lib/odweta/\n\
\n\
.PHONY: build run\n\
\n\
default: build\n\
\n\
build: ");               // src/main.c # add files to compile here\n
  for (int i = 0; i < src_files_num; i++) {
    strcat(makefile, "src/");
    strcat(makefile, src_files[i]);
    strcat(makefile, " ");
  }
  strcat(makefile, "\n\
\t@$(CC) $(CFLAGS) -o ./build/main $^ $(PATCH) $(INCLUDEDIR) $(LIBSPATH) $(LIBS)\n\
\n\
run:\n\
\t@./build/main\n");

  char *ptr = makefile;

  return ptr;
}

void write_new_makefile(char *makefile) {
  FILE *fd = fopen("Makefile", "w");

  fprintf(fd, "%s", makefile);

  fclose(fd);
}

void save_build_status(int status_code) {
  FILE *fd = fopen("./build/.build_status", "w");
  if (!fd) {
    fprintf(stderr, "Invalid permissions. Could not open ./build/.build_status "
                    "for writing.\n");
  }

  fprintf(fd, "%d", status_code);

  fclose(fd);
}

void build_project() {
  /* iterate through all files in the ./<project_name>/src/ directory
   * create a new makefile with these files incuded for compilation
   * run make
   * if the build was successfull -> print something like 'the build was
   * successfull'
   */

  char cwd[2048];
  getcwd(cwd, sizeof(cwd));

  char src_dir[strlen(cwd) + 4 + 1];
  strcpy(src_dir, cwd);
  strcat(src_dir, "/src");

  src_files_num = count_src_files(src_dir);

  char src_files[src_files_num][1000];

  load_filenames(src_files, src_dir);

  char *makefile = create_new_makefile(src_files);

  write_new_makefile(makefile);

  if (system("make") != 0) {
    fprintf(stderr, "An error occured while building.\n");
    exit(1);
  }
}
