CC := gcc
FILES := src/main.c src/args.c src/new.c src/build.c src/run.c

build:
	gcc -o main $(FILES)
	sudo cp main /usr/bin/vendetta
	rm -fr main

run:
	vendetta new thing
	ls
